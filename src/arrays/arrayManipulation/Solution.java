package arrays.arrayManipulation;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[] arr = new int[n];
        while ( m-- > 0 ) {
            int a = in.nextInt()-1;
            int b = in.nextInt();
            int k = in.nextInt();
            arr[a] += k;
            if ( b < arr.length ) arr[b] -= k;
        }
        int highest = 0;
        int current = 0;
        for ( int i=0; i<arr.length; i++ ) {
            current += arr[i];
            if ( current > highest ) highest = current;
        }
        System.out.println(highest);
    }
}
