package arrays.arraysDS;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for ( int i=0; i<n; i++ ) {
            arr[n-1-i] = in.nextInt();
        }
        StringBuilder out = new StringBuilder();
        for ( int i : arr ) out.append(i + " ");
        System.out.print(out.toString().trim());
    }
}