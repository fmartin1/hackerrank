package arrays.dynamicArray;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int Q = in.nextInt();
        int lastAnswer = 0;
        List<Integer>[] seqList = new List[N];
        while ( Q-->0 ) {
            int type = in.nextInt();
            int x = in.nextInt();
            int y = in.nextInt();
            int i_seq = (x ^ lastAnswer) % N;
            if ( seqList[i_seq] == null ) {
                seqList[i_seq] = new ArrayList<Integer>();
            }
            switch (type) {
                case 1:
                    seqList[i_seq].add(y);
                    break;
                case 2:
                    lastAnswer = seqList[i_seq].get(y % seqList[i_seq].size());
                    System.out.println(lastAnswer);
                    break;
            }
        }
    }
}
