package arrays.leftRotation;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int d = in.nextInt();
        int i_arr = 0;
        int[] arr = new int[n];
        do {
            int index = (i_arr - d < 0) ? (n + i_arr - d) : (i_arr - d);
            arr[index] = in.nextInt();
        } while ( ++i_arr < n );
        for ( int i : arr ) {
            System.out.print(i + (--i_arr == 0 ? "\n" : " "));
        }
    }
}
