package arrays.sparseArrays;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        String[] strings = new String[N];
        while ( --N >= 0 ) {
            strings[N] = in.next();
        }
        int Q = in.nextInt();
        while ( Q--> 0 ) {
            query(in.next(), strings);
        }
    }

    private static void query(String query, String[] strings) {
        int count = 0;
        for ( String string : strings ) if ( string.equals(query) ) count++;
        System.out.println(count);
    }
}
