package arrays.twoDArrayDS;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[][] arr = getArray(in);
        List<HourGlass> liHg = getHourGlasses(arr);
        HourGlass highest = null;
        for ( HourGlass hg : liHg ) {
            if ( highest == null || hg.getValue() > highest.getValue() ) highest = hg;
        }
        System.out.println(highest.getValue());
    }

    private static List<HourGlass> getHourGlasses(int[][] arr) {
        List<HourGlass> list = new ArrayList<>();
        for ( int row=0; row<4; row++ ) {
            for ( int col=0; col<4; col++ ) {
                list.add(new HourGlass(arr,row,col));
            }
        }
        return list;
    }

    private static int[][] getArray(Scanner in) {
        int size = 6;
        int[][] arr = new int[size][size];
        for ( int i=0; i<size; i++ ) {
            for ( int j=0; j<size; j++ ) {
                arr[i][j] = in.nextInt();
            }
        }
        return arr;
    }

    private static class HourGlass {
        public static final int HEIGHT = 3;
        public static final int WIDTH = 3;
        private int value;

        public HourGlass(int[][] arr, int row, int col) {
            for ( int i=0; i<3; i++ ) {
                value += arr[row][i+col];
                value += arr[row+2][i+col];
            }
            value += arr[row+1][col+1];
        }

        public int getValue() {
            return value;
        }
    }
}