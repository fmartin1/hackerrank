package iceCreamParlor;

/**
 * Created by ferna on 10/3/2017.
 */
import java.io.*;
import java.util.*;

public class Solution {

    private static class Visit {
        int m;
        int n;
        int[] f;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        Visit[] visits = new Visit[t];
        for ( int i=0; i<t; i++ ) {
            visits[i] = new Visit();
            visits[i].m = in.nextInt();
            visits[i].n = in.nextInt();
            visits[i].f = new int[visits[i].n];
            for ( int j=0; j<visits[i].n; j++ ) visits[i].f[j] = in.nextInt();
        }
        StringBuilder result = new StringBuilder();
        for ( Visit v : visits ) result.append(getIceCreamsPurchased(v) + "\n");
        System.out.println(result.toString());
    }

    private static String getIceCreamsPurchased(Visit v) {
        int f1 = 0;
        int f2 = 0;

        for ( int i = 0; i < v.f.length ; i++ ) {
            if ( v.f[i] < v.m - 1 ) {
                f1 = i+1;
                for ( int j = i+1; j < v.f.length; j++ ) {
                    if ( v.m - v.f[i] - v.f[j] == 0 ) {
                        f2 = j+1;
                        return f1 + " " + f2;
                    }
                }
            }
        }
        return "failed at life";
    }
}