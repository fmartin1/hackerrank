package knightL;

/**
 * Created by ferna on 10/3/2017.
 * possible algorithm>
 * for y calculate module of n spaces left in x and vice-versa
 * ty = leftInX > 0 ? : leftInX % speedY : 1
 * tx = leftInY > 0 ? : leftInY % speedX : 1
 * then
 * speedY = speedY * (-1ty)
 * speedX = speedX * (-1tx)
 *
 * if Knight can move AND -> move
 * else invertSpeeds (e.g. (-1,2) -> (2,-1))
 *
 *
 * (1,2) can move?
 1,2
 3 % 2 = 1
 2 % 1 = 0
 (-1,2) can move?
 0,4
 4%2 = 2
 0%1 = 0
 (1,2) can move?
 (2,-1) can move?
 2,3
 2%2 = 0
 1%1 = 0
 (1,2) can move?
 (2,1) can move?
 4,4



 (1,3) can move?
 1,3
 3%3 = 0
 1%1 = 0
 (1,3) can move?
 (3,1) can move?
 3,3


 (1,4) can move?
 1,4
 3%4 = 3
 0%1 = 0
 (-1,4) can move?
 (4,-1) can move?
 (-4,1) can move?
 (1,-4) can move?
 2,0
 2%4 = 2
 4%1 = 0
 (1,4) can move?
 3,4
 1%4 = 1
 0%1 = 0
 (-1,4) can move?
 (4,-1) can move?
 (-4,1) can move?
 (1,-4) can move?
 4,0
 4%4 = 0
 0%1 = 0
 (1,4) can move?
 (4,1) can move?
 (-4,-1) can move?
 (-1,-4) can move?
 (1,-4) can move?
 (-4,1) can move?
 0,1
 4%4 = 0
 3%1 = 0
 (1,4) can move?
 (4,1) can move?
 4,2
 0%4 = 0
 2%1 = 0
 (1,4) can move?
 (4,1)
 (-4,1)
 */
import java.util.*;

public class Solution {

    private static class Knight {
        final int speedY;
        final int speedX;
        private int tmpSpdY;
        private int tmpSpdX;
        int positionY;
        int positionX;

        Knight(int spdY, int spdX) {
            positionY = 1;
            positionX = 1;
            speedY = spdY;
            speedX = spdX;
            resetSpeed();
        }

        private void resetSpeed() {
            tmpSpdY = speedY;
            tmpSpdX = speedX;
        }

        void invertSpeed() {
            int tmp = tmpSpdY;
            tmpSpdY = tmpSpdX;
            tmpSpdX = tmp;
        }

        void calculateNewDirection(int leftInY, int leftInX) {
            if ( leftInY < 0 || ( (leftInY % tmpSpdX) & 1) != 0 ) tmpSpdY *= -1;
            if ( leftInX < 0 || ( (leftInX % tmpSpdY) & 1) != 0 ) tmpSpdX *= -1;
        }

        void move() {
            positionY += tmpSpdY;
            positionX += tmpSpdX;
        }

        public boolean canMove(int n) {
            if ( positionX + speedX > 0 && positionX + speedX <= n &&
                    positionY + speedY > 0 && positionY + speedY <= n )
                return true;
            return false;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String output = getPossibleMovesInBoard(n);
        System.out.println(output);
    }

    private static String getPossibleMovesInBoard(int n) {
        StringBuilder output = new StringBuilder();
        int row = 1;
        int col = 1;
        while ( row < n ) {
            col = 1;
            while ( col < n ) output.append( getMinimalMoves(n,row,col) + (col++ == n ? "\n" : " ") );
            row++;
        }
        return output.toString();
    }

    private static int getMinimalMoves(int n, int y, int x) {
        Knight k = new Knight(y,x);
        int moves = 0;
        while ( k.positionX != n || k.positionY != n ) {
            if ( k.canMove(n) ) {
                k.move();
                if ( k.positionX == 1 && k.positionY == 1 ) return -1;
                moves++;
                k.calculateNewDirection(n-k.positionY, n-k.positionX);
            } else {
                k.invertSpeed();
                if ( !k.canMove(n) ) k.calculateNewDirection(n-1-k.positionY, n-1-k.positionX);
            }
        }
        return moves;
    }

}