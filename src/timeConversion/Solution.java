package timeConversion;

/**
 * Created by ferna on 10/3/2017.
 */



import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static String timeConversion(String s) {
        // Complete this function
        boolean pm = s.substring(s.length() - 2).toUpperCase().equals("PM");
        int hour = Integer.parseInt(s.split(":")[0]);
        if ( pm ) hour += (hour == 12) ? 0:12;
        else if ( hour == 12 ) hour = 0;
        String hour_s = hour < 10 ? "0" + hour : "" + hour;
        return hour_s + s.substring(2, s.length() - 2);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String result = timeConversion(s);
        System.out.println(result);
    }
}